package com.appserver.handler;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

import com.appserver.pojo.AppSerStatus;
import com.appserver.utils.StaticConstants;
import com.sprouts.spm_framework.utils.ConfigUtils;
import com.sprouts.spm_framework.utils.Logger;

/**
 * 采集完成后用以发送数据给中心节点
 * 
 * @author Administrator
 * 
 */
public class DataWriter implements Runnable {

    private InetSocketAddress inetSocketAddress;
    private AppSerStatus status;
    private Logger logger = new Logger();

    public DataWriter(AppSerStatus status, String center_machine) {
        this.status = status;
        inetSocketAddress =
                new InetSocketAddress(center_machine, Integer.parseInt(ConfigUtils
                        .getValue(StaticConstants.NIO_SERVER_PORT)));
    }

    public void run() {
        send();
    }

    public void send() {
        try {
            SocketChannel socketChannel = SocketChannel.open(inetSocketAddress);
            socketChannel.configureBlocking(false);
            ByteBuffer byteBuffer = ByteBuffer.allocate(512);
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(byteStream);
            out.writeObject(this.status);
            out.flush();
            socketChannel.write(ByteBuffer.wrap(byteStream.toByteArray()));
            while (true) {
                int readBytes = socketChannel.read(byteBuffer);
                if (readBytes > 0) {
                    byteBuffer.flip();
                    logger.info("Response is : " + new String(byteBuffer.array(), 0, readBytes));
                    socketChannel.close();
                    break;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
