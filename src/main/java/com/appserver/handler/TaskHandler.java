package com.appserver.handler;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ScheduledFuture;

import com.appserver.jconsole.DataCollectThread;
import com.appserver.pojo.AppSerTask;
import com.appserver.utils.CachedService;
import com.appserver.utils.StaticConstants;
import com.sprouts.spm_framework.utils.AsyncTaskUtils;
import com.sprouts.spm_framework.utils.ConfigUtils;
import com.sprouts.spm_framework.utils.Logger;

/**
 * 使用nio接收从中心接收的任务
 * 
 * @author howson
 * 
 */
public class TaskHandler implements Runnable {

    private Logger logger = new Logger();
    private Map<AppSerTask, ScheduledFuture> taskMap = new HashMap<AppSerTask, ScheduledFuture>();
    private InetSocketAddress inetSocketAddress;
    private NioHandler handler = new ServerHandler();
    private AppSerTask task = null;
    private AsyncTaskUtils asyncTaskHandler;

    public TaskHandler() {
        inetSocketAddress =
                new InetSocketAddress(Integer.parseInt(ConfigUtils
                        .getValue(StaticConstants.NIO_CLIENT_PORT)));
        asyncTaskHandler = AsyncTaskUtils.getInstance();
    }

    public void run() {
        try {
            // 如果缓存队列里有任务，则先恢复任务
            if (!CachedService.taskQueue.isEmpty()) {
                for (AppSerTask aptask : CachedService.taskQueue) {
                    DataCollectThread thread = new DataCollectThread(aptask);
                    taskMap.put(aptask,
                            asyncTaskHandler.dispatchScheduleTask(thread, 5, aptask.getFrequency()));
                }
                CachedService.taskQueue.clear();
            }

            Selector selector = Selector.open(); // 打开选择器
            ServerSocketChannel serverSocketChannel = ServerSocketChannel.open(); // 打开通道
            serverSocketChannel.configureBlocking(false); // 非阻塞
            serverSocketChannel.socket().bind(inetSocketAddress);
            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT); // 向通道注册选择器和对应事件标识
            logger.info("Server: socket server started.");
            while (true) { // 轮询
                int nKeys = selector.select();
                if (nKeys > 0) {
                    Set<SelectionKey> selectedKeys = selector.selectedKeys();
                    Iterator<SelectionKey> it = selectedKeys.iterator();
                    while (it.hasNext()) {
                        SelectionKey key = it.next();
                        if (key.isAcceptable()) {
                            logger.info("Server: SelectionKey is acceptable.");
                            handler.handleAccept(key);
                        } else if (key.isReadable()) {
                            logger.info("Server: SelectionKey is readable.");
                            handler.handleRead(key);
                        } else if (key.isWritable()) {
                            logger.info("Server: SelectionKey is writable.");
                            handler.handleWrite(key);
                        }
                        it.remove();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * 服务端事件处理实现类
     */
    class ServerHandler implements NioHandler {

        public void handleAccept(SelectionKey key) throws IOException {
            ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
            SocketChannel socketChannel = serverSocketChannel.accept();
            logger.info("Server: accept client socket " + socketChannel);
            socketChannel.configureBlocking(false);
            socketChannel.register(key.selector(), SelectionKey.OP_READ);
        }


        public void handleRead(SelectionKey key) throws IOException {
            ByteBuffer byteBuffer = ByteBuffer.allocate(512);
            SocketChannel socketChannel = (SocketChannel) key.channel();
            while (true) {
                int readBytes = socketChannel.read(byteBuffer);
                if (readBytes > 0) {
                    String taskdata = new String(byteBuffer.array(), 0, readBytes);
                    logger.info("Server: data = " + taskdata);
                    if (taskdata.startsWith("0")) { // 取消监控任务
                        task = stringToTask(taskdata);
                        if (taskMap.get(task) != null) {
                            taskMap.get(task).cancel(true);
                            CachedService.removeTask(task);
                        }
                    } else if (taskdata.startsWith("1")) { // 心跳包
                        socketChannel.write(ByteBuffer.wrap("i am fine,sir".getBytes()));
                        break;
                    } else if (taskdata.startsWith("2")) { // 创建新监控任务
                        task = stringToTask(taskdata);
                        if (!taskMap.containsKey(task)) {
                            DataCollectThread thread = new DataCollectThread(task);
                            taskMap.put(
                                    task,
                                    asyncTaskHandler.dispatchScheduleTask(thread, 5,
                                            task.getFrequency()));
                            CachedService.putToTaskQueue(task);
                            CachedService.cachedToLocal();
                        }
                        socketChannel.write(ByteBuffer.wrap("i've received,sir".getBytes()));
                        break;
                    }
                }
            }
            socketChannel.close();
        }

        public void handleWrite(SelectionKey key) throws IOException {
            ByteBuffer byteBuffer = (ByteBuffer) key.attachment();
            byteBuffer.flip();
            SocketChannel socketChannel = (SocketChannel) key.channel();
            socketChannel.write(byteBuffer);
            if (byteBuffer.hasRemaining()) {
                key.interestOps(SelectionKey.OP_READ);
            }
            byteBuffer.compact();
        }

        public AppSerTask stringToTask(String data) {
            String cols[] = data.split("\\*");
            if (cols.length > 0) {
                return new AppSerTask(cols[1], Integer.parseInt(cols[2]), cols[3],
                        Integer.parseInt(cols[4]), cols[5]);
            }
            return null;
        }
    }

}
