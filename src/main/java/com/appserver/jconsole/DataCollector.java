package com.appserver.jconsole;

import static java.lang.management.ManagementFactory.MEMORY_MXBEAN_NAME;
import static java.lang.management.ManagementFactory.OPERATING_SYSTEM_MXBEAN_NAME;
import static java.lang.management.ManagementFactory.RUNTIME_MXBEAN_NAME;
import static java.lang.management.ManagementFactory.THREAD_MXBEAN_NAME;
import static java.lang.management.ManagementFactory.newPlatformMXBeanProxy;

import java.io.IOException;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.lang.management.OperatingSystemMXBean;
import java.lang.management.RuntimeMXBean;
import java.lang.management.ThreadMXBean;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import com.appserver.pojo.AppSerStatus;
import com.appserver.pojo.AppSerTask;

public class DataCollector {

    private SnapshotMBeanServerConnection server = null;
    private String jmxURL = null;
    private MBeanServerConnection mbsc = null;
    private JMXConnector connector = null;

    private JMXServiceURL serviceURL;
    private Map<String, String[]> map;
    private boolean hasPlatformMXBeans = false;

    private ThreadMXBean threadMBean = null;
    private MemoryMXBean memoryMBean = null;
    private OperatingSystemMXBean operatingSystemMBean = null;
    private RuntimeMXBean runtimeMBean = null;
    private com.sun.management.OperatingSystemMXBean sunOperatingSystemMXBean = null;

    private static CPUSummary cpuSummary = null;

    private String host;
    private int port;

    public interface SnapshotMBeanServerConnection extends MBeanServerConnection {
        /**
         * Flush all cached values of attributes.
         */
        public void flush();
    }

    public static class Snapshot {
        private Snapshot() {}

        public static SnapshotMBeanServerConnection newSnapshot(MBeanServerConnection mbsc) {
            final InvocationHandler ih = new SnapshotInvocationHandler(mbsc);
            return (SnapshotMBeanServerConnection) Proxy.newProxyInstance(
                    Snapshot.class.getClassLoader(),
                    new Class[] {SnapshotMBeanServerConnection.class}, ih);
        }
    }

    public synchronized ThreadMXBean getThreadMXBean() throws IOException {
        if (hasPlatformMXBeans && threadMBean == null) {
            threadMBean = newPlatformMXBeanProxy(server, THREAD_MXBEAN_NAME, ThreadMXBean.class);
        }
        return threadMBean;
    }

    public synchronized MemoryMXBean getMemoryMXBean() throws IOException {
        if (hasPlatformMXBeans && memoryMBean == null) {
            memoryMBean = newPlatformMXBeanProxy(server, MEMORY_MXBEAN_NAME, MemoryMXBean.class);
        }
        return memoryMBean;
    }

    public synchronized OperatingSystemMXBean getOperatingSystemMXBean() throws IOException {
        if (hasPlatformMXBeans) {
            operatingSystemMBean =
                    newPlatformMXBeanProxy(server, OPERATING_SYSTEM_MXBEAN_NAME,
                            OperatingSystemMXBean.class);
        }
        return operatingSystemMBean;
    }

    @SuppressWarnings("restriction")
    public synchronized com.sun.management.OperatingSystemMXBean getSunOperatingSystemMXBean()
            throws IOException {
        try {
            ObjectName on = new ObjectName(OPERATING_SYSTEM_MXBEAN_NAME);
            if (server.isInstanceOf(on, "com.sun.management.OperatingSystemMXBean")) {
                sunOperatingSystemMXBean =
                        newPlatformMXBeanProxy(server, OPERATING_SYSTEM_MXBEAN_NAME,
                                com.sun.management.OperatingSystemMXBean.class);
            }
        } catch (InstanceNotFoundException e) {
            return null;
        } catch (MalformedObjectNameException e) {
            return null; // should never reach here
        }
        return sunOperatingSystemMXBean;
    }

    public synchronized RuntimeMXBean getRuntimeMXBean() throws IOException {
        if (hasPlatformMXBeans) {
            runtimeMBean = newPlatformMXBeanProxy(server, RUNTIME_MXBEAN_NAME, RuntimeMXBean.class);
        }
        return runtimeMBean;
    }

    public DataCollector(AppSerTask task) {
        this.host = task.getHostname();
        this.port = task.getPort();
        jmxURL = "service:jmx:rmi:///jndi/rmi://" + this.host + ":" + this.port + "/jmxrmi";
        map = new HashMap<String, String[]>();
        String[] credentials = new String[] {"monitorRole", "tomcat"};
        map.put("jmx.remote.credentials", credentials);

    }

    public void tryConnect() throws IOException {
        try {
            serviceURL = new JMXServiceURL(jmxURL);
            connector = JMXConnectorFactory.connect(serviceURL, map);
            mbsc = connector.getMBeanServerConnection();
            server = Snapshot.newSnapshot(mbsc);
            ObjectName on = new ObjectName(THREAD_MXBEAN_NAME);
            hasPlatformMXBeans = server.isRegistered(on);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public double retrieveCpuUsage() throws IOException {
        cpuSummary.updateCPUInfo(getOperatingSystemMXBean(), getRuntimeMXBean(),
                getSunOperatingSystemMXBean());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        tryConnect();
        return cpuSummary.updateCPUInfo(getOperatingSystemMXBean(), getRuntimeMXBean(),
                getSunOperatingSystemMXBean());
    }

    public AppSerStatus collect() {
        AppSerStatus status = null;
        try {
            cpuSummary = new CPUSummary();
            this.tryConnect();
            ThreadMXBean tmBean = this.getThreadMXBean();
            MemoryMXBean memoryBean = this.getMemoryMXBean();

            double cpuUsage = this.retrieveCpuUsage();
            int tlCount = tmBean.getThreadCount();
            long ttCount = tmBean.getTotalStartedThreadCount();
            long cpuCount = tmBean.getCurrentThreadCpuTime();
            MemoryUsage u = memoryBean.getHeapMemoryUsage();
            long memUsed = u.getUsed();
            float men_usage = memUsed / 1024 / 1024;
            status = new AppSerStatus();
            status.setAlive_thread(tlCount);
            status.setCpu_time(cpuCount);
            status.setTotal_thread(ttCount);
            status.setCpu_usage(cpuUsage);
            status.setHost(host);
            status.setPort(port);
            status.setMem_usage(men_usage);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return status;
    }

    static class SnapshotInvocationHandler implements InvocationHandler {

        private final MBeanServerConnection conn;
        private Map<ObjectName, NameValueMap> cachedValues = newMap();
        private Map<ObjectName, Set<String>> cachedNames = newMap();

        @SuppressWarnings("serial")
        private static final class NameValueMap extends HashMap<String, Object> {}

        SnapshotInvocationHandler(MBeanServerConnection conn) {
            this.conn = conn;
        }

        synchronized void flush() {
            cachedValues = newMap();
        }

        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            final String methodName = method.getName();
            if (methodName.equals("getAttribute")) {
                return getAttribute((ObjectName) args[0], (String) args[1]);
            } else if (methodName.equals("getAttributes")) {
                return getAttributes((ObjectName) args[0], (String[]) args[1]);
            } else if (methodName.equals("flush")) {
                flush();
                return null;
            } else {
                try {
                    return method.invoke(conn, args);
                } catch (InvocationTargetException e) {
                    throw e.getCause();
                }
            }
        }

        private Object getAttribute(ObjectName objName, String attrName) throws MBeanException,
                InstanceNotFoundException, AttributeNotFoundException, ReflectionException,
                IOException {
            final NameValueMap values =
                    getCachedAttributes(objName, Collections.singleton(attrName));
            Object value = values.get(attrName);
            if (value != null || values.containsKey(attrName)) {
                return value;
            }
            return conn.getAttribute(objName, attrName);
        }

        private AttributeList getAttributes(ObjectName objName, String[] attrNames)
                throws InstanceNotFoundException, ReflectionException, IOException {
            final NameValueMap values =
                    getCachedAttributes(objName, new TreeSet<String>(Arrays.asList(attrNames)));
            final AttributeList list = new AttributeList();
            for (String attrName : attrNames) {
                final Object value = values.get(attrName);
                if (value != null || values.containsKey(attrName)) {
                    list.add(new Attribute(attrName, value));
                }
            }
            return list;
        }

        private synchronized NameValueMap getCachedAttributes(ObjectName objName,
                Set<String> attrNames) throws InstanceNotFoundException, ReflectionException,
                IOException {
            NameValueMap values = cachedValues.get(objName);
            if (values != null && values.keySet().containsAll(attrNames)) {
                return values;
            }
            attrNames = new TreeSet<String>(attrNames);
            Set<String> oldNames = cachedNames.get(objName);
            if (oldNames != null) {
                attrNames.addAll(oldNames);
            }
            values = new NameValueMap();
            final AttributeList attrs =
                    conn.getAttributes(objName, attrNames.toArray(new String[attrNames.size()]));
            for (Attribute attr : attrs.asList()) {
                values.put(attr.getName(), attr.getValue());
            }
            cachedValues.put(objName, values);
            cachedNames.put(objName, attrNames);
            return values;
        }

        private static <K, V> Map<K, V> newMap() {
            return new HashMap<K, V>();
        }
    }

}
