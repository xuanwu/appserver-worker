package com.appserver.jconsole;

import com.appserver.handler.DataWriter;
import com.appserver.pojo.AppSerStatus;
import com.appserver.pojo.AppSerTask;

public class DataCollectThread implements Runnable {

    private DataCollector collector;
    private String center_machine;

    public DataCollectThread(AppSerTask task) {
        collector = new DataCollector(task);
        this.center_machine = task.getCenter_machine();
    }

    public void run() {
        AppSerStatus status = collector.collect();
        DataWriter writer = new DataWriter(status, center_machine);
        writer.run();
    }

}
