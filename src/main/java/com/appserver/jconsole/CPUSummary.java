package com.appserver.jconsole;

import java.lang.management.OperatingSystemMXBean;
import java.lang.management.RuntimeMXBean;

public class CPUSummary {

    private CPUOverview cpuOverview;

    public CPUSummary() {
        cpuOverview = new CPUOverview();
    }

    public static class Result {
        long upTime = -1L;
        long processCpuTime = -1L;
        long timeStamp;
        int nCPUs;
    }

    @SuppressWarnings("restriction")
    public double updateCPUInfo(OperatingSystemMXBean osMBean, RuntimeMXBean rmBean,
            com.sun.management.OperatingSystemMXBean sunOSMBean) {
        Result result = new Result();
        result.upTime = rmBean.getUptime();
        result.processCpuTime = sunOSMBean.getProcessCpuTime();
        result.nCPUs = osMBean.getAvailableProcessors();
        result.timeStamp = System.currentTimeMillis();
        return cpuOverview.updateCPUInfo(result);
    }

    private static class CPUOverview {

        private long prevUpTime, prevProcessCpuTime;

        CPUOverview() {

        }

        public double updateCPUInfo(Result result) {
            double cpuUsage = 0.00;
            if (prevUpTime > 0L && result.upTime > prevUpTime) {
                // elapsedCpu is in ns and elapsedTime is in ms.
                long elapsedCpu = result.processCpuTime - prevProcessCpuTime;
                long elapsedTime = result.upTime - prevUpTime;
                // cpuUsage could go higher than 100% because elapsedTime
                // and elapsedCpu are not fetched simultaneously. Limit to
                // 99% to avoid Plotter showing a scale from 0% to 200%.
                cpuUsage = Math.min(99F, elapsedCpu / (elapsedTime * 10000F * result.nCPUs));
            }
            this.prevUpTime = result.upTime;
            this.prevProcessCpuTime = result.processCpuTime;
            return cpuUsage;
        }
    }

}
