package com.appserver.main;

import com.appserver.handler.TaskHandler;
import com.appserver.utils.CachedService;
import com.appserver.utils.StaticConstants;
import com.sprouts.spm_framework.utils.AsyncTaskUtils;
import com.sprouts.spm_framework.utils.ConfigUtils;

public class AppServerWorker {

    private static AsyncTaskUtils asyncHandler;

    /**
     * 启动任务监听器，启动前先检查是否有本地任务
     */
    public static void main(String[] args) {

        // 初始化配置
        ConfigUtils.initExternalConfig(StaticConstants.CONFIG_PATH);

        // 先检查是否有本地任务
        CachedService.retrieveTaskList();

        asyncHandler = AsyncTaskUtils.getInstance();
        asyncHandler.dispatchTask(new TaskHandler());
    }

}
