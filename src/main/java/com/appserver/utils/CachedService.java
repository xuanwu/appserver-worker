package com.appserver.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.appserver.pojo.AppSerTask;
import com.sprouts.spm_framework.utils.Logger;

/**
 * This class caches the appserver tasks to local file so that the tasks can be recovered right away
 * after an accident termination. Also considered to cache data when internet connection failed.
 */

public class CachedService {

    public static BlockingQueue<AppSerTask> taskQueue = new LinkedBlockingQueue<AppSerTask>();
    private static Logger logger = new Logger();

    /**
     * 把任务持久化到本地文件，以方便程序崩溃时恢复任务
     */
    public static void cachedToLocal() {
        for (AppSerTask task : taskQueue) {
            String line =
                    task.getHostname() + "*" + task.getPort() + "*" + task.getType() + "*"
                            + task.getFrequency() + "*" + task.getCenter_machine();
            writeCached(line);
        }
        taskQueue.clear();
    }

    public static void writeCached(String cache) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("taskcached.dat", true));
            writer.write(cache);
            writer.write("\n");
            writer.close();
        } catch (IOException e) {
            logger.error("write cache error:", e);
        }
    }

    /**
     * 删除本地持久化任务文件中的该任务
     * 
     * @param task
     */
    public static void removeTask(AppSerTask task) {
        BufferedReader reader = null;
        BufferedWriter writer = null;
        try {
            reader = new BufferedReader(new FileReader("taskcached.dat"));
            StringBuffer buffer = new StringBuffer(8192);
            String lineRemove =
                    task.getHostname() + "*" + task.getPort() + "*" + task.getType() + "*"
                            + task.getFrequency() + "*" + task.getCenter_machine();
            String line = null;
            while ((line = reader.readLine()) != null) {
                if (line.contains(lineRemove)) continue;
                buffer.append(line);
                buffer.append("\n");
            }
            reader.close();

            writer = new BufferedWriter(new FileWriter("taskcached.dat"));
            writer.write(buffer.toString());
            writer.close();
        } catch (IOException e) {
            logger.error("remove local task error", e);
        }
    }

    /**
     * 程序重启时恢复任务
     */
    public static void retrieveTaskList() {
        try {
            File file = new File("taskcached.dat");
            if (file.exists()) {
                BufferedReader reader = new BufferedReader(new FileReader("taskcached.dat"));
                String line = null;
                while ((line = reader.readLine()) != null) {
                    String[] cols = line.split("\\*");
                    if ((!line.isEmpty()) && cols.length == 5) {
                        AppSerTask task =
                                new AppSerTask(cols[0], Integer.parseInt(cols[1]), cols[2],
                                        Integer.parseInt(cols[3]), cols[4]);
                        taskQueue.put(task);
                    }
                }
                reader.close();
            }
        } catch (FileNotFoundException e) {
            logger.error("", e);
        } catch (IOException e) {
            logger.error("", e);
        } catch (InterruptedException e) {
            logger.error("", e);
        }
    }

    public static void putToTaskQueue(AppSerTask task) {
        try {
            taskQueue.put(task);
        } catch (InterruptedException e) {
            logger.error("", e);
        }
    }

}
