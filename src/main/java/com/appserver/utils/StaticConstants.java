package com.appserver.utils;

public class StaticConstants {

    public static String NIO_CLIENT_PORT = "AppServer.nio.client-port";
    public static String NIO_SERVER_PORT = "AppServer.nio.server-port";

    public static String CONFIG_PATH = "../config/appserver-config.xml";
}
